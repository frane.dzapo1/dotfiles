"***************************************************************************
" Table of content
"***************************************************************************
" - Initial Setup
" - Vim-Plug 
"   - Initialization
"   - Plugins List
"   - Plugins Setup
" - Basic Setup
"   - Encoding
"   - Mouse
"   - Shell
"   - Folding
"   - Spaces & Tabs
" - Visual Settings
"   - Colors
"   - UI Config
"   - Statusline
"   - Windows
"   - Searching
" - Abbreviations
" - Keymappings
"   - Basic Navigation
"   - Splitting
"   - Session Management
"   - Buffer navigation
"   - Tabs
" - Commands
" - Custom Functions

"***************************************************************************
"***************************************************************************
"***************************************************************************
" Initial Setup
"***************************************************************************
"{{{ Inital Setup 
" highlight last inserted text
" nnoremap gv `[v`]

" define leader key
let mapleader="'"                       " map leader to ;

" Prompt for a command to run
map <Leader>vp :VimuxPromptCommand<CR>

" Copy/Paste/Cut
if has('unnamedplus')                   " if a version of vim supports +clipboard
    set clipboard=unnamed,unnamedplus   " yank & delete copy to system clipboard as well
endif

noremap YY "+y<CR>
noremap <leader>p "+gP<CR>
noremap XX "+x<CR>
"}}}


"***************************************************************************
" Vim-Plug Initialization
"***************************************************************************
"{{{Vim-Plug core
let vimplug_exists=expand('~/.vim/autoload/plug.vim')
if has('win32')&&!has('win64')
    let curl_exists=expand('C:\Windows\Sysnative\curl.exe')
else
    let curl_exists=expand('curl')
endif

" NOT used at all, a remnent of Vim bootstrap
let g:vim_bootstrap_langs = "python,javascript,go,html"
let g:vim_bootstrap_editor = "vim"

if !filereadable(vimplug_exists)
    if !executable(curl_exists)
        echoerr "You have to install curl or first install vim-plug yourself!"
        execute "q!"
    endif
    echo "Installing Vim-Plug..."
    echo ""
    silent exec "!"curl_exists" -fLo " . shellescape(vimplug_exists) . " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
    let g:not_finish_vimplug = "yes"

    autocmd VimEnter * PlugInstall
endif

" Required:
call plug#begin('~/.vim/plugged')
"}}}

"===========================================================================
" Plugins List
"===========================================================================
"{{{ Plugins List
"File explorer
" - NERDTree                ('scrooloose/nerdtree')
"Terminal
" - dbext                   ('vim-scripts/dbext.vim')
" - vimux                   ('benmills/vimux')
" - vimproc                 ('shougo/vimproc.vim')
"Git
" - fugitive                ('tpope/vim-fugitive')
" - rhubarb                 ('tpope/vim-rhubarb')
"Project-level navigation
" - tagbar                  ('majutsushi/tagbar')
" - grep                    ('yegappan/grep')
" - fzf                     ('junegunn/fzf', 'junegunn/fzf.vim')
"Snippets
" - Ultisnips               ('sirver/ultisnips')
" - vim-snippets            ('honza/vim-snippets')
"UI
" - CSApprox                ('vim-scripts/CSApprox')
" - vim-airline             ('vim-airline/vim-airline')
" - vim-airline-themes      ('vim-airline/vim-airline-themes')
" - vim-gitgutter           ('airblade/vim-gitgutter')
"Editing
" - GOYO                    ('junegunn/goyo.vim')
" - delimitmate             ('raimondi/delimitmate')
" - NERD Commenter          ('scrooloose/nerdcommenter')
" - Tabular                 ('godlygeek/tabular')
"Markdown
" - Vim Markdown            ('plasticboy/vim-markdown')
"Programming Languages
" - vim-polyglot            ('sheerun/vim-polyglot')
" - ALE                     ('w0rp/ale')
"Python
" - jedi-vim                ('davidhalter/jedi-vim')
" - requirements            ('raimon49/requirements.txt.vim')
" - PEP8 Indent             ('Vimjas/vim-python-pep8-indent')
" - c.vim                   ('vim-scripts/c.vim')
" - split-manpage           ('ludwig/split-manpage.vim')
" - vim-go                  ('fatih/vim-go')
" - vim-css3-syntax         ('hail2u/vim-css3-syntax')
" - vim-coloresque          ('gko/vim-coloresque')
" - vim-haml                ('tpope/vim-haml')
" - emmet-vim               ('mattn/emmet-vim')
" - vim-javascript-syntax   ('jelera/vim-javascript-syntax')
"}}}

" Plugins Setup ############################################################
"{{{
"{{{ NERDTree
" File explorer inside Vim
Plug 'scrooloose/nerdtree'

let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['node_modules','\.rbc$','\~$','\.pyc$','\.db$','\.sqlite$','__pycache__','\.swp$']
let g:NERDTreeSortOrder=['^__\.py$','\/$','*','\.swp$','\.bak$','\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
let g:NERDTreeWinSize=50
let g:NERDTreeQuitOnOpen=1
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite,*node_modules/
nnoremap <silent> <F2> :NERDTreeFind<CR>
nnoremap <silent> <F3> :NERDTreeToggle<CR>
"}}}

" Terminal interaction          ############################################
"{{{ dbext
Plug 'vim-scripts/dbext.vim'
"}}}

"{{{ vimux
" Interacting with TMux inside Vim
Plug 'benmills/vimux'
"}}}

"{{{ vimproc
" https://github.com/shougo/vimproc.vim
" asynchronous execution library
let g:make='gmake'
if exists('make')
        let g:make = 'make'
endif

Plug 'shougo/vimproc.vim', {'do': g:make}
"}}}

" Git ======================================================================
"{{{ vim-fugitive
" Git commands inside Vim
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'

" vim-fugitive aliases
noremap <Leader>ga :Gwrite<CR>
noremap <Leader>gc :Git commit --verbose<CR>
noremap <Leader>gsh :Git push<CR>
noremap <Leader>gll :Git pull<CR>
noremap <Leader>gs :Git<CR>
noremap <Leader>gb :Git blame<CR>
noremap <Leader>gd :Gvdiffsplit<CR>
noremap <Leader>gr :GRemove<CR>
" Open current line on GitHub
nnoremap <Leader>o :.Gbrowse<CR>
"}}}


" Project-level navigation      ############################################
"{{{ Tagbar
" https://github.com/preservim/tagbar
Plug 'majutsushi/tagbar'

nmap <silent> <F4> :TagbarToggle<CR>
let g:tagbar_autofocus = 1
"}}}

"{{{ grep
" Integrate various grep like seach tools (grep, fgrep, ag, ack, ...)
Plug 'yegappan/grep'

nnoremap <silent> <leader>f :Rgrep<CR>
let Grep_Default_Options = '-IR'
let Grep_Skip_Files = '*.log *.db'
let Grep_Skip_Dirs = '.git node_modules'
"}}}

"{{{ fzf
if isdirectory('/usr/local/opt/fzf')
    Plug '/usr/local/opt/fzf' | Plug 'junegunn/fzf.vim'
else
    Plug 'junegunn/fzf', {'dir': '~/.fzf', 'do': './install --bin'}
    Plug 'junegunn/fzf.vim'
endif

set wildmode=list:longest,list:full
set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
"}}}


" Snippets                      ############################################
"{{{ Snippets
Plug 'sirver/ultisnips'
Plug 'honza/vim-snippets'

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"
let g:UltiSnipsEditSplit="vertical"
"}}}


" UI                            ############################################
"{{{ CSApprox
Plug 'vim-scripts/CSApprox'
"}}}

"{{{ vim-airline + airline themes
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

let g:airline_powerline_fonts=1

if !exists('g:airline_symbols')
    let g:airline_symbols={}
endif
if !exists('g:airline_powerline_fonts')
  let g:airline#extensions#tabline#left_sep = ' '
  let g:airline#extensions#tabline#left_alt_sep = '|'
  let g:airline_left_sep          = '▶'
  let g:airline_left_alt_sep      = '»'
  let g:airline_right_sep         = '◀'
  let g:airline_right_alt_sep     = '«'
  let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
  let g:airline#extensions#readonly#symbol   = '⊘'
  let g:airline#extensions#linecolumn#prefix = '¶'
  let g:airline#extensions#paste#symbol      = 'ρ'
  let g:airline_symbols.linenr    = '␊'
  let g:airline_symbols.branch    = '⎇'
  let g:airline_symbols.paste     = 'ρ'
  let g:airline_symbols.paste     = 'Þ'
  let g:airline_symbols.paste     = '∥'
  let g:airline_symbols.whitespace = 'Ξ'
else
  let g:airline#extensions#tabline#left_sep = ''
  let g:airline#extensions#tabline#left_alt_sep = ''

  " powerline symbols
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
endif
"}}}

"{{{ vim-gitgutter
" Show git file changes (a.k.a. diff)
Plug 'airblade/vim-gitgutter' 
"}}}


" Editing                       ############################################
"{{{ Goyo
" Distraction free writing!
Plug 'junegunn/goyo.vim'
"}}}

"{{{ delimitmate
" Automatic closing of quotes, parenthesis, brackets, etc.
" https://github.com/raimondi/delimitmate
Plug 'raimondi/delimitmate'
"}}}

"{{{ Nerd Commenter
" Powerful commenting functions
" https://github.com/preservim/nerdcommenter
Plug 'preservim/nerdcommenter'
"}}}

"{{{ Tabular
" Aligning text (requirement for plasticboy/vim-markdown)
" https://github.com/godlygeek/tabular
Plug 'godlygeek/tabular'
"}}}


" Markdown +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"{{{ Vim Markdown
" https://github.com/preservim/vim-markdown
" Plug 'plasticboy/vim-markdown'
"}}}


" Programing Languages =====================================================

" Syntax Highlighting for multitude of languages
"{{{ vim-polyglot
" Collection of language packs
" https://github.com/sheerun/vim-polyglot
Plug 'sheerun/vim-polyglot'
"}}}

" Code Linting
"{{{ ALE - Asynchronous Lint Engine
" Client for LSP server to provide linting
" https://github.com/dense-analysis/ale
Plug 'w0rp/ale'

let g:ale_linters = {}
:call extend(g:ale_linters, {
    \"go": ['golint', 'go vet'], })

:call extend(g:ale_linters, {'python': ['flake8', 'pylint'], }) 

" use Google YAPF linter for fixing (a.k.a. `:ALEFix`)
let g:ale_fixers = { 'python': ['yapf'], }

nmap <F10> :ALEFix<CR>
let g:ale_fix_on_save = 1 " automatically fix a file on save
"}}}

" Python ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"{{{ jedi-vim
" binding to the autocomplete library Jedi
" https://github.com/davidhalter/jedi-vim
Plug 'davidhalter/jedi-vim'

augroup vimrc_python
    autocmd!
    autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=79
        \ formatoptions+=croq softtabstop=4
        \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

let g:jedi#popup_on_dot = 0
let g:jedi#goto_assignments_command = "<leader>g"
let g:jedi#goto_definitions_command = "<leader>d"
let g:jedi#documentation_command = "K"
let g:jedi#usages_command = "<leader>n"
let g:jedi#rename_command = "<leader>r"
let g:jedi#show_call_signatures = "0"
let g:jedi#completions_command = "<C-Space>"
let g:jedi#smart_auto_mappings = 0
"}}}

"{{{ requirements.txt.vim
" Requirements File syntax support
Plug 'raimon49/requirements.txt.vim', {'for': 'requirements'}
"}}}

"{{{ vim-python-pep8-indent
" https://github.com/Vimjas/vim-python-pep8-indent
Plug 'Vimjas/vim-python-pep8-indent'
"}}}

" Syntax highlight
let python_highlight_all = 1

" C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"{{{ c.vim
Plug 'vim-scripts/c.vim', {'for': ['c', 'cpp']}
"}}}

"{{{ slip-manpage
Plug 'ludwig/split-manpage.vim'
"}}}

" Go +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"{{{ syntax highlighting, 'GoImport', 'GoRun', GoTo, ...
Plug 'fatih/vim-go', {'do': ':GoInstallBinaries'}

function! s:build_go_files()
    let l:file - expand('%')
    if l:file =~# '^\f\+_test\.go$'
        call go#test#Test(0, 1)
    elseif l:file =~# '^\f\+\.go$'
        call go#cmd#Build(0)
    endif
endfunction

let g:go_list_type = 'quickfix'
let g:go_fmt_command = 'goimports'
let g:go_fmt_fail_silently = 1

let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_structs = 1
let g:go_highlight_generate_tags = 1
let g:go_highlight_space_tab_errors = 0
let g:go_highlight_array_whitespace_error = 0
let g:go_highlight_trailing_whitespace_error = 0
let g:go_highlight_extra_types = 1

autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4

augroup completion_preview_close
    autocmd!
    if v:version > 703 || v:version == 703 && has('patch598')
        autocmd CompleteDone * if !&previewwindow && &completeopt =~ 'preview' | silent! pclose | endif
    endif
augroup END

augroup go

    au!
    au Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
    au Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
    au Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
    au Filetype go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')

    au FileType go nmap <Leader>dd <Plug>(go-def-vertical)
    au FileType go nmap <Leader>dv <Plug>(go-doc-vertical)
    au FileType go nmap <Leader>db <Plug>(go-doc-browser)

    au FileType go nmap <leader>r <Plug>(go-run)
    au FileType go nmap <leader>t <Plug>(go-best)
    au FileType go nmap <Leader>gt <Plug>(go-coverage-toggle)
    au FileType go nmap <Leader>i <Plug>(go-info)
    au FileType go nmap <silent> <Leader>i <Plug>(go-metalinter)
    au FileType go nmap <C-g> :GoDecls<cr>
    au FileType go nmap <leader>dr :GoDeclsDir<cr>
    au FileType go imap <C-g> <esc>:<C-u>GoDecls<cr>
    au FileType go imap <leader>dr <esc>:<C-u>GoDeclsDir<cr>
    au FileType go nmap <leader>rb :<C-u>call <SID>build_go_files()<CR>

augroup END
"}}}

" HTML +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"{{{ CSS3 syntax support
Plug 'hail2u/vim-css3-syntax'
"}}}

"{{{ colors preview in editor
Plug 'gko/vim-coloresque'
"}}}

"{{{ tpope/vim-haml
Plug 'tpope/vim-haml'
"}}}

"{{{ Emmet commands for HTML generation
Plug 'mattn/emmet-vim'
"}}}


" JavaScript +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"{{{ jelera/vim-javascript-syntax
Plug 'jelera/vim-javascript-syntax'
"}}}

"}}}

"***************************************************************************
"***************************************************************************
" Include user's extra bundle
if filereadable(expand("~/.vimrc.local.bundles"))
    source ~/.vimrc.local.bundles
endif

call plug#end()

" turn on filetype detection, allow for loading of language specific indentation files and plugins
" (~/.vim/indent/python.vim & ~/.vim/plugin/python.vim)
if has('filetype')
  filetype indent plugin on " load filetype-specific indent files
endif

"***************************************************************************
" Basic Setup
"***************************************************************************
"{{{
set hidden              " allow switching between files even when there are unsaved changes to the current (hide the current when switching instead of demanding user to save)
set noswapfile          " disable swap file
set confirm             " ask for confirmation for certain actions


" Encoding =================================================================
"{{{
set encoding=utf-8      " text encoding inside the buffer
set fileencoding=utf-8  " text encoding when writing into a file (defult: same as 'encoding')
set fileencodings=utf-8
set ttyfast             " faster and smoother Vim experience
"}}}

" Mouse ====================================================================
"{{{
set mouse+=a             " mouse enable in all modes
set mousemodel=popup    " what right click does
set t_Co=256
set guioptions=egmrti   " for GUI Vim
set gfn=Monospace\ 10

" enable mouse drage split resizing with tmux
if &term =~ '^screen'
    " tmux knows the extended mouse mode
    set ttymouse = xterm2
endif
"}}}

" Shell ====================================================================
"{{{
" What shell to use when :!
if exists('$SHELL')
    set shell=$SHELL
else
    set shell=/bin/sh
endif
"}}}

" Folding ==================================================================
"{{{
set foldenable          " enable folding
set foldcolumn=0        " display open and closed folds in the gutter
set foldlevelstart=10   " open most folds by default (only very nested code blocks are folded as start)
set foldmethod=manual   " fold based on manual setting (marker, manual, expr, syntax, diff)

" space in normal mode open/closes folds (space not needed for navigation)
nnoremap <space> za
"}}}

" Spaces & Tabs ============================================================
"{{{
set tabstop=4       " number of visual spaces per TAB (when reading TAB from file)
set softtabstop=0   " number of spaces in tab when editing (spaces inserted whan TAB is pressed)
set shiftwidth=4    " number of spaces used for '>>' and '<<'
set expandtab       " tabs are spaces (TABs essentially become an alias for N spaces)
set autoindent      " indent newline if current line is indented 
set backspace=indent,eol,start " FIX backspace indent
"}}}

"}}}
"***************************************************************************
" Visual Settings
"***************************************************************************
"{{{

" Colors ===================================================================
"{{{
if has("gui_running")   " GUI Vim
    if has("gui_mac") || has("gui_macvim")
        set guifont=Menlo:h12
        set transparency=7
    endif
else                    " Terminal Vim
    if $COLORTERM == 'gnome-terminal'
        set term=gnome-256color
    else
        if $TERM == 'xterm'
            set term=xterm-256color
        endif
    endif
endif

if &term =~ '256color'
    set t_ut=
endif

colorscheme onedark " colorscheme
"--------------------------------------------------------
" 'enable' VS 'on' - 'enable' keeps your current color settings, while 'on' will
" overrule your color settings
if has('syntax')
  syntax enable " enable syntax processing
endif
"}}}

" UI Config ================================================================
"{{{
function! ToggleLineNumber()
    if v:version > 703
        set norelativenumber!
    endif
    set nonumber!
endfunction
map <leader>h :call ToggleLineNumber()<CR>
set number              " display line number
set cursorline          " highlight current line
set ruler               " display line & column cursor position (bottom right)
set showmatch           " highlight matching [{()}]

set showcmd             " show command in bottom bar
set wildmenu            " visual autocomplete for command menu

set gcr=a:blinkon0      " Disable the blinking cursor
set scrolloff=3         " when to stop scrolling, a.k.a. can you scroll past the end of the file

" cursor style
let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"
"}}}

" Statusline ===============================================================
"{{{
set laststatus=2        " keep status line on all windows 'always'
set cmdheight=2         " hight of command-line

" number of lines checked for set commands
set modeline
set modelines=10

set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

if exists("*fugitive#statusline")
set statusline+=%{fugitive#statusline()}
endif

" vim-airline
let g:airline_theme = 'powerlineish'
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline_skip_empty_sections = 1

let g:airline#extensions#virtualenv#enabled = 1
"}}}

" Windows ==================================================================
"{{{
set title
set titleold="Terminal"
set titlestring=%F
"}}}

" Searching ================================================================
"{{{
set ignorecase "do case insensitive search
set smartcase " case-sensitive if search pattern contains a capital character
set incsearch " search as characters are entered
set hlsearch " highlight matches

" Center on the search result when cycling through search results
nnoremap n nzzzv
nnoremap N Nzzzv
" turn off search highlight - keymap
nnoremap <silent> <leader><space> :nohlsearch<CR>
"}}}

"}}}
"***************************************************************************
"" Abbreviations
"***************************************************************************
"{{{
" enable capital letters for most common command save/quit (w/q)
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall
"}}}

"***************************************************************************
" Keymappings
"***************************************************************************
" highlight last inserted text
nnoremap gV `[v`]

" Basic Navigation =========================================================
"{{{
" move vertically by visual line
nnoremap j gj
nnoremap k gk

" Set working directory
nnoremap <leader>. :lcd %:p:h<CR>
noremap <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>

" Opens a tab edit command with teh path of the currenly edited file filled
noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>

" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
"}}}

" Splitting ================================================================
"{{{
" create a horlizontal and vertical split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>
" Splits navigation - no need to use <C-W>
nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-L> <C-W>l
nnoremap <C-H> <C-W>h
set splitbelow " create new vertical split below
set splitright " create new horizontal split right

" QuickKey - new buffer in new vertical split
nnoremap <silent> vv <C-w>

" Split resizing

"}}}

" Session Management =======================================================
"{{{
let g:session_directory= "~/.vim/session"
let g:session_autoload = "no"
let g:session_autosave = "no"
let g:session_command_aliases = 1

nnoremap <leader>so :OpenSession<Space>
nnoremap <leader>ss :SaveSession<Space>
nnoremap <leader>sd :DeleteSession<CR>
nnoremap <leader>sc :CloseSession<CR>
"}}}

" Buffer nav ===============================================================
"{{{
noremap <leader>z :bp<CR>
noremap <leader>q :bp<bar>sp<bar>bn<bar>bd<CR>
noremap <leader>x :bn<CR>
" Close buffer
noremap <leader>c :bd<CR>
"}}}

" Tabs =====================================================================
"{{{
" nnoremap <Tab> gt
" nnoremap <S-Tab> gT
nnoremap <silent> <S-t> :tabnew<CR>

" terminal emulation
nnoremap <silent> <leader>sh :terminal<CR>
"}}}

"***************************************************************************
" Commands
"***************************************************************************
" remove trailing whitespaces
command! FixWhitespace :%s/\s\+$//e

"***************************************************************************
" Custom Functions
"***************************************************************************
if !exists('*s:setupWrapping')
    function s:setupWrapping()
        set wrap
        set wm=2
        set textwidth=79
    endfunction
endif

"***************************************************************************
" Autocmd Rules
"***************************************************************************
" The PC is fast enough, do syntax highlight syncing from start unless 200
augroup vimrc_sync_fromstart
    autocmd!
    autocmd BufEnter * :syntax sync maxlines=200
augroup END

" Remember cursor position
augroup vimrc_remember_cursor_position
    autocmd!
    autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

" txt
augroup vimrc_wrapping
    autocmd!
    autocmd BufRead, BufNewFile *.txt call s:setupWrapping()
augroup END

" make/cmake
augroup vimrc_make_cmake
    autocmd!
    autocmd FileType make setlocal noexpandtab
    autocmd BufNewFile, BufRead CMakeLists.txt setlocal filetype=cmake
augroup END

set autoread

" set foldmethod=syntax for Markdown files
autocmd FileType markdown setlocal foldmethod=syntax

" Define Marakdown header levels as folds
autocmd FileType markdown syn region markdownHeader start="^#\+" end="$" fold transparent

"***************************************************************************
" Macros
"***************************************************************************

" File Navigation ==========================================================
" Search down into subfolders
set path+=**
command! MakeTags !ctags -R .

" Tweaks for browsing
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'

"***************************************************************************
"***************************************************************************
" Include user's local vim config
if filereadable(expand("~/.vimrc.local"))
    source ~/.vimrc.local
endif
"***************************************************************************
"***************************************************************************
