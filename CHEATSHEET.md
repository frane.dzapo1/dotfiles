# My Developent Cheatsheet

## Editing & Navigation
### Basic navigation
 - char (left/down/up/right) = `h/j/k/l`
 - word (prev begin/next end/next begin) = `b/e/w`
 - WORD (prev begin/next end/next begin) = `B/E/W`
 - Line (begin/first char/last char/end) = `0/^/g_/$`
 - Paragraph (back/forward) = `{/}`
 - Screen (begin/middle/end) = `H/M/L`
 - by Screen (back full/back half/forward half/forward full) = `<C-b>/<C-u>/<C-d>/<C-f>`
 - File (begin/n-th line/n-th percentage/end) = `gg/NG/N%/G`

### Searching
 - In line searching
   - `f<char>` = find char, `;/,` go to next/previous occurance
 - In file searching
   - `/<pattern>` = find pattern, `n/N` go to next/previous occurance
   - `*/#` = next/previous word under cursor
   - `%` = matching braces
 - In project searching
 - `vim +/<pattern> <filename>` = open <filename> and go to <pattern>
 - 

 - open existing buffer in new vertical/horizontal split
 - **Marks**
 - **Folding**

## Dev Views

## Searching

## Git

## Running + Debugging
