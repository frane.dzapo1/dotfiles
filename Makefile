neovim: tree-sitter ## Configure neovim
	@./scripts/neovim.sh configure
tree-sitter: ## Install tree-sitter
	@./scripts/tree-sitter.sh install
