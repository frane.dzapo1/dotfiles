# dotfiles

## Description
Configuration files for work tools (tmux, vim, zsh, ...) across all my machines.

## Installation

### Requirements


1. You must manually install all of the tools whose configuration is located in this repository.
    - [Install Zshell](https://gist.github.com/derhuerst/12a1558a4b408b3b2b6e#file-linux-md)
        `sudo apt update`
        `sudo apt upgrade`
        `sudo apt install zsh`
        
    - Check if zsh is installed: `zsh --version`
    - Set zshell as your default shell: `chsh -s $(which zsh)`
        - this command changes the entry inside `/etc/passwd` which specifies the shell for each user (among other things)

---
## Installation
Manually instll all of the tools whose configuration is located in this repository
 1. Install the prerequisite apps (**Zshell**, **Tmux**, **Vim**)
    - Zshell installation
    - Tmux installation
    - Vim installation

 2. Clone the repository into your $HOME directory
 3. Create the simbolic links of the config files (`.zshrc`, `.tmux.conf`, `.vimrc`) so that any change you make in dotfiles repository are imediately used as config files (no need to copy your changes all of the time)
    - `ln -s ~/dotfiles/.zshrc ~/.zshrc`
    - `ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf`
    - `ls -s ~/dotfiles/.vimrc ~/.vimrc`

## Zsh
After installation of zshell and other setup steps there are several things you should know to use Zshell effectively.

For configuration and custamization of Zshell we are going to use **OhMyZshell** framework for configuration management.
Installation of OhMyZshell:
- Download `install.sh` script for installing OhMyZshell = `$ sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`
- Run `sh install.sh`

## OhMyZshell plugins



### Vim
**Prerequisite:** [install Vim]
 1. Create symbolic link of `.vimrc` directory inside the `dotfiles` into the $HOME directory (this is where Vim expects to find the `.vimrc` file)j
        ```bash
        sudo apt update
        sudo apt upgrade
        sudo apt install zsh
        ```
        - Check if zsh is installed: `zsh --version`
        - Set zshell as your default shell: `chsh -s $(which zsh)`


---
## Installation
 1. Install the prerequisite apps ( **Zshell**, **Tmux**, **Vim** )
 2. Clone the repository into your $HOME directory
 3. Create the simbolic links of the config files (`.zshrc`, `.tmux.conf`, `.vimrc`)

