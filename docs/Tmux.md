# Tmux


## Configuration details


## Usage

### Basic usage
Command mode: `<prefix>:`

#### Sessions
 - Create new session: `tmux` | `:new`(inside tmux)
   - with name: `tmux new -s {name}` | `:new -s {name}`
 - Kill session: `tmux kill-ses -t {name}`
 - List sessions: `tmux ls` | `<prefix>s`
 - Rename session: `<prefix>$`
 - Detach from a session: `<prefix>d`
 - Attach to a session: `tmux a -t {name}`
   - to last session: `tmux a`
 - Move to last/previous session: `<prefix>(` | `<prefix>)`

**Windows**
 - Create window: `<prefix>c`
 - Close window: `<prefix>&`
 - Rename window: `<prefix>,`
 - Move between windows:
   - Next/Previous: `<prefix>n`/`<prefix>p`
   - Last active: `<prefix>l`
   - To index: `<prefix>[0..9]`

**Panes**
 - Move between panes:
   - 

### Tmux templated sessions
1. Oneoff-task
2. Always-running
3. Project
    - (1) editor + shell
    - (2) server process
    - (3) additional processes, workers
    - (.) anything additionaly required

### Seamlessly switching between contexts
Usage of Tmux sessions allows you to startup applications and open tools for one specific work context, then easily detach from than context while still leaving it running in the background while you switch to a different work context. 

Contexts can mean different projects, or different aspects/domains of one project.
For example:
 - one context in which you have a running server, dislay of running logs, ...
 - other context, your source code opened in 

### Automated creation of dev env
Every developer, before they start the actual work, must setup their local development environment. This generally consists of:
 - open terminal
 - `cd` to the project directory
 - open an editor
 - start a REPL
 - start a build or compilation process
 - start few services
 - tail a log file or two
With `tmux` upi cam automate the startup of a customized, terminal-based dev environment.

Ultimately this will result in:
 - less context switching
 - more organized development process
 - less time spent reorganizing windows and tabs



#### Codified convention

### Terminal buffer searching and copying
Since Vi-mode is enabled in tmux, by pressing `prefix [` inside a tmux terminal, you enter a *Copy mode*.
In copy mode you can navigate through the console history just like you would navigate through a file in Vim, and by pressing `v` you start selecting text and finish by pressing `y`.


