# Zshell
Zshell is an alternative (improvement) to the default Bash shell. It provides numerous improvements and benefits.

## Installation
1. Install Zshell
    ```bash
    sudo apt-get update
    sudo apt-get install zsh
    ```
2. (Optional) Set Zshell as your default shell
    Check where is your zshell installed: `which zsh`. Once you get a location of your zshell installation execute `chsh -s <zshell_location>` to set Zshell as your default shell

3. (Optional) Install ZSH configuration manager
    One of the great benefits of Zshell is that it can be heavely extended and customized. Insted of doing all the configuring manually yourself, using a configuration manager makes things a lot easier.
    There are to popular configuration managers for Zshell, *OhMyZSH* and *Prezto*. This instructions only cover how to install *OhMyZSH*.
    ```bash
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    ```
    - This installation script will move your previous Zshell configuration from `.zshrc` file into a `.pre-oh-my-zsh` file. So any of your existing configurations you want to keep, transfer from `.pre-oh-my-zsh` into the new `.zshrc`.

## Configuration
Resources:
 - https://grml.org/zsh/zsh-lovers.html
 - 

Various instances of zsh interpreter are differentiated into **interactive**/**non-interactive** and **login**/**non-login**.
Interactive shell is the one you type into, and non-interactive shell is the one executing commands from a script.
Login shell is the interactive shell started on startup, or explicitly started with an `-l` option (`zsh -l`), while any other explicitly called shell (`zsh`) either interactive or non-interactive is non-login shell.

Startup files:
Startup files are located either in `/etc` or in `~` home directory of a user. Files in `/etc` are run for all users, while files in `~` are separate for each individual user.
- `/etc/zshenv` - always run for every zsh
- `~/.zshenv` - *usually* run for every zsh
- `/etc/profile` - run for login shells
- `~/.zshprofile` - run for login shells
- `/etc/zshrc` - run for interactive shells
- `~/.zshrc` - run for interactive shells
- `/etc/zlogin` - run for login shells
- `~/.zlogin` - run for login shells

### Aliases
**Sufix aliases** (`alias -s`)
It allows you to specify a app to use for opening various file extensions.
Example: `alias -s tex=vim` - .tex files are opened with vim

**Global aliases** (`alias -g`)
Global aliases can be used anywhere in command line. More explicitly it can be used inside other commands.
Example: `alias -g C='| wc -l'` = "C" at the end of a command will pip its output into "word count":

### Completion

---
**Oh-My-Zsh configurations**
:
### OHMZ themese
*Oh-My-Zsh* comes bundled with a bunch of themes out of the box. They are located in `~/.oh-my-zsh/themes` folder. Custom themes can also be installed.
Look at screenshots of available themes at https://github.com/ohmyzsh/ohmyzsh/wiki/themes.
Select one of the themes by simply setting a `ZSH_THEME` variable inside the `.zshrc` file. Example: `ZSH_THEME="robyrussel"`. 

### OHMZ plugins
*Oh My ZSH* comes with and manages plugins for you that can extend and customize the behaviour of Zshell in all kinds of ways. They are located in `~/.oh-my-zsh/plugins`. Custom plugins can also be installed.
Look at available plugins at https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins.
To enable a plugin simply add its name to the `plugins=(git, ...)` line inside the `.zshrc` file.

**Git** (https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/git#aliases)
    Adds a lot of useful git aliases and several functions.
    Aliases:
        - `ga` = `git add`
        - `gaa` = `git add --all`
        - ...
    Functions:
        - `current_branch` = print a name of a current branch
        - `grename <old> <new>` = rename `<old>` branch to `<new>`, including in origin remote

## Features and Usage

- **Better completion** (`cd`, `git`,
  Show available options underneath.
- **Path expansion**
  Allows you to simply write first letters of each dir in a path and then by simply pressing Tab expand this abriviated path into a exact path you want.

