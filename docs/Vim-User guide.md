# My User guide
----------------------------------------
## Table of Content
 - Tips
 - Concepts
 - [x] Motions
    - Jumping to specific line
    - Window moving
    - Find/Search for string
 - Operators (Editing commands)
    - Text Objects
    - Undo/Redo
    - Delete
    - Change
    - Yank
 - File manipuation
 - Marks
 - Folding
     - Creating and Deleting folds
     - Opening and Closing folds
     - Moving over folds
 - Tag jumping
 - Sessions
----------------------------------------
## Tips
1. Get comfotable with the repeat `.` command. Use this frequently to speed up your work.

2. `N<command>` - repeat the command N times. Commonly used for:
    - `Ndd`: delete N lines
    - `Np`: paste N times

3. Use `c` instead of `di` whenever your goal is to change text. Develop a habit of distingusing a delete intent, from change intent.
    - Keep in mind: `cw` works like `ce` (legacy behaviour that remained)

4. Use `r` command for fixing single character typos. It is faster then going into "insert" mode and then going back to "normal" mode.

5. Get comfortable using visual modes (normal visual, line visual, block visual). Don't waste time and thoughts figuring out what the right move commands is.
    - `v` / `V` 

6. Move Vim to background/foreground.
    - `Ctrl-z`/`fg` - background/foreground

7. Zone selection throught the use of Visual Mode and Text Objects 
    - `vi"` - select the string
    - `v2iw` - select 2 words
   You can repeat text object commands (`aw`, `iw`, ...) inside the Visual Mode to expand your selection.

----------------------------------------
## Concepts
**Motions** are commands move the currsor around, but more importantly they can be used after the *operator* command, to have the command operate on the text that was moved over (text between the cursor position before and after the motion).

**Operators** are commands that either change or delete text. 
Following operators are available:
 - `c`:     change
 - `d`:     delete
 - `y`:     yank into register
 - `~`:     swap case
 - `gu`:    make lowercase
 - `!`:     filter through an external program
 - `=`:     filter through 'equalprg' or C-indenting if empty
 - `gq`:    text formatting
 - `gw`:    text formatting with no cursor movement
 - `g?`:    ROT13 encooding
 - `>`:     shift right
 - `<`:     shift left
 - `zf`:    define a fold
 - `g@`:    call function set with the 'operatorfunc' option

*Linewise/Characterwise*: Each operator either affects while lines, or the characters between the start and end position. Generally, motions that move between lines affect lines, and motions that move within a line affect characters. *Blockwise* motion selects a ractangular piece of text between the starting cursor position (upper left corner of the block) and end cursor position (bottom right corner of the block).
 - Using the `v`/`V` you can force the other type (characterview/linewise)
    - `dj` deletes two lines (linewise)
    - `dvj` deletes from the starting positino to the below end position (characterwise)
 - Use `<C-V>` after the operator and before the motion to force a blo
    - `d<C-V>j` deletes the character under the cursor and the character below the cursor (blockwise)
*Exclusive/Inclusive*: Character motion is either inclusive or exclusive. 
Inclusive, meaning start and end position of the motion are included in the operation. 
Exclusive, means that the last character is not included.

**Text line/Screen line** Text line represents a piece of text between to new-line (`\n`) characters. Screen-line is a single line of text as it is presented on the screen. Some commands operate on the text lines and some on screen lines. For example, `0` moves the cursor to the begining of a text line, while `g0`  moves the cursor to the begining of the screen-line.

**Sentence** is defined as ending with `.`, `!` or `?`, followed by either the end of a line, space or tab. Any number of "closing characters"; `)`, `]`, `"`, `'` can appear after "ending character" and white-space characters. *Paragraph and section boundary is also a sentence boundary**

**Paragraph** begins after each empty line, and also at each of a set of *paragraph macros*.

**Section** begins after "form-feed" in the first colum, and also at each of a set of *paragraph macros*.

**Buffer** is an area of Vim's memory used to hold text read from a file. Buffer can be unassociated with any file to allow the entry of text. We edit text inside the buffer and consequently save the final changes to the file of our choice.

**Registers** are named variables (memory locations) into which you can copy, cut text into and paste text from. They provide a more efficient way of copy-paste. Instead of going repetedly back and forth between the file you want to copy from and file you want to copy into, you can simply copy multiple sections in multiple different registers, all while stying within the source file. And than which to destination file and paste from specific registers all in one go.

**Window** is a section of a the buffer that is seen on the screen.

##  Motions
**Left-right inline** motions move the cursor to the specified column in the current line. They stop at the first column and at the end of the line.
- `h`/`l`                   left/right exclusive motion
- `0`/`$`                   move to **first/last character in line**
    - `g0`/`g$`             move to first/last character of the **screen-line**
- `^`/`g_`                  move to **first/last non-blank character in line**
- `{perc}gM`              move to the **middle of the line** ("10gM" = move to 10 percent of line )
    - `gm`                move to the **middle of the screen-line** 
- `{col_num}|`            move to precisely **{col_num} character in the line**
- `f{char}`/`F{char}`       move to the **next/previous occurance of {char}** (exclusive)
- `t{char}`/`T{char}`       move to **just before the next/previous occurance of {char}** (inclusive)

**Up-down** motions
- `k`/`j`                   **up/down lines** motion
- `gk`/`gj`                 move **up/down screen-lines**
- `-`/`+`                   move **up/down to the first non-blank character in line**
- `gg`/`G`                  **go to first/last line**
- `{count}G`/`:{count}`     go to **{count} line**
- `{perc}%`               go to **percentage {perc} of lines**

**Word/WORD** motions are enable you to jump around words/WORDS. Words are pieces of text, sequences of keyword characters that are separated by non-keyword characters. WORDS are seqences of non-blank characters separated by whitespace characters.
 - `{count}w`/`{count}W`    move **{count} words/WORDS forward** to word start (exclusive)
 - `{count}e`/`{count}E`    move **{count} words/WORDS forward to end** (inclusive)
 - `{count}b`/`{count}B`    move **{count} words/WORDS backward** to word start (exclusive)
 - `{count}ge`/`{count}gE`  move **{count} words/WORDS backward** to word end (inclusive)

**Text object** motions. Text objects are the following 3 kinds of objects. 
*Sentence*, a sequence of characters ending with a end-character (`.`/`!`/`?`) followed by a whitespace-character (`space`/`newline`/`tab`), also any of the `)`/`]`/`"`/`'` characters between an end-character and whitespace-character. 
*Paragraph* begins after each empty line or any of "paragraph macros" specified in `paragraphs` option. 
*Section* begins after a "form-feed" in the first colum or any of the "section macros" specified in the `secions` option.
 - `(`/`)`                  move **sentence** backward/forward (exclusive)
 - `{`/`}`                  move **paragraph** backward/forward (exclusive)
 - `[[`/`]]`                move **sections** backward/forward
 - `[]`/`][`                move **section** backward/forward

**Parenthesis** (`()`, `[]`, `{}`) surounded text block 
 - `%`:               

### Selection motions
A series of commands that can only be used while *in Visual mode* or *after an operator*. 
Commands in this series are separated into "inner" (i) commands that doesn't include surounding whitespaces and "outer" (a) commands that include surrounding whitespaces.

 - `aw`/`iw`/`aW`/`iW`          select a word/WORD that include/exlude spaces between the words
 - `as`/`is`                select a sentence
 - `ap`/`ip`                select a paragraph
 - `a]`/`a[`/`i]`/`i[`          select a `[]` block
 - `a)`/`a(`/`i)`/`i(`          select a `()` block
 - `a<`/`a>`/`i<`/`i>`          select a `<>` block
 - `at`/`it`                select a "tag" block
 - ...

## Jumps
"Jump" is a command that normally moves the cursor several lines away. A position of the cursor before a jump is remember, so you can return to it with the "''" command and "\`\`" command.
Following are jump commands:
 - `'`
 - \`
 - `G`
 - `/`
 - `?`
 - `n`
 - `N`
 - `%`                      go to *matching parenthesis* (or first closing)
 - `(`
 - `)`
 - `[[`
 - `]]`
 - `{`
 - `}`
 - `:s`
 - `:tag`
 - `L`
 - `M`
 - `H`

Two main commands for navigating the jumpings history are:
 - `CTRL-O`/`CTRL-I`            go to older/newer cursor position in jump list
The jump list of cursor positions can be printed or cleared:
 - `:ju[mps]`/`:cle[arjumps]`   print/clear the jump list

### Jumping to specific line
**Screen relative jumping**
- `H`/`M`/`L`:              jump to top/middle/bottom of the screen

**Line specific jumping**
- `{line num}G`:        go to {line num} line
- `G`/`gg`:               go to last/first line
- `{length percent}%`:  move to {length percent} line
- `zz`:                 set current line to the middle of a window

### Window moving
- `Ctrl-e`/`Ctrl-y`:      scroll window *one line* down/up
- `Ctrl-u`/`Ctrl-d`:      scroll *half a window* up/down
- `Ctrl-f`/`Ctrl-b`:      scrool *whole window* down/up

### Find/Search for string
Search patterns are writen useing the Regular Expression syntax.
- `/{string}`:          search for string
    - `{count}n`/`{count}N`:        next/previous match
- `*`/`#`:                find next/previous occurance of current word
- `g*`/`g#`:              partial match the word under cursor

## Operators (Editing commands)
Operators are co mmands that are combined with a movement (`l`,`w`,`)`, ...). This (usually) executes an operations on the section of text from starting cursor position to final cursor position. Some movements are *inclusive* and some are *exclusive*. Meaning, do they include or not the character under the cursor.
For example, `w` (next word) is exclusive, while `e` (end of word) is inclusive.

Core operators:
 - `d`:                 delete (cut)
 - `c`:                 change (`d` delete + `i` Insert Mode)
 - `y`:                 yank (copy)
 - `p`:                 put
 - `=`:                 format
 - `g~`:                toggle case (`gu`-lowercase, `gU`-uppercase)
 - `>`:                 add indentation
 - `<`:                 remove indentation

### Text Objects
Text Objects are text fragments as they are recognized by Vim, that can be used in combination with operators. Basic Text Objects are **word**, **sentence**, and **paragraph**. In addition to these, there are programming language specific objects, plus you can also define your own.

Use text objects by typing `<command>a<object>` or `<command>i<object>`. `a` includes white spaces surrounding the object, and `i` does not include surrounding white spaces.

Basic object:
 - `w`: word
 - `s`: sentence
 - `p`: paragraph
 - `"`: double quoted string
 - `'`: single quoted string
 - `)`: parenthesized block
 - `]`: bracketed block
 - `}`: brace block
 - `t`: markup language tag block (<h2> ... </h2>)
 - `>`: markup lanaguge tag

### Undo/Redo
 - `u`:                  Undo
 - `Ctrl+r`:             Redo

### Delete
 - `d{motion}`           DELETE cursor moves over
 - `x`/`X`                 delete a **character** after/before currsor
 - `dd`                  delete an **entire line**
 - `D`                   delete **to the end of line**

*Common usages*
 - `dw`:                 delete a word
 - `d}`:                 delete an entire paragraph

### Change
 - `c<motion>`:          delete and enter Insert mode
 - `C`:                  change everyting to the end of line
 - `s`:                  delete a single and enter Insert mode
 - `S`(`cc`):              change a whole line

### Yank 



## File manipulation (save,load,...)
All editing is done on the **buffer**. You need to save to the file to commit the changes. 
Optionally, you can edit configuratoin (.vimrc) to enable autosave on every buffer change.

 - `:e <path/to/file>`: open a file
 - `:w`: save changes
 - `:saveas <path/to/file>`
 - `:e!`: throw away changes and keep editing
 - `:wq`|`:x`: save and quit


## Marks
Navigate through Jump history. `{line}G` command, `/` search command
 - `Ctrl-o`/`Ctrl-i` = older/newer jump location

Jump between Marks ("\`")
 - `:marks` - list all marks
 - \` - last position
 - \`. - last edit position
 - `m{char}` - place a mark
 - \`{char} - jump to named {char} mark
 - `'{char}` - jump to named {char} mark line
 - `'` - cursor position before doing a jump
 - `"` - cursor position when last editing the file
 - `[` - start of last change
 - `]` - end of last change
 
##Folding

### Creating and Deleting folds
**'foldmethod'** option specifies a way in which folds are defined withing a file/buffer.
Fold method options:
 1. *manual* = folds are "manually" defined
 2. *indent* = folds are defined by **indent**
 3. *expr* = you specify a custom **expression** that defines a fold
 4. *syntax* = folds are defined by **syntax** highlighting
 5. *diff* = **unchanged text** define the fold
 6. *marker* = **marker** inside the text define the fold

**Manual** fold making consists of manually calling fold defining commands:
- zf{motion}    = create fold
- zF            = crete fold for lines
- zd            = delete one fold at the cursor
- zD            = delete folds recursively at the cursor
- zE            = eliminate all folds in the window
All specified folds are lost when you abandon the file, unless you save the folds in the *View* (`:mkview`, `:loadview`).

**Indent** option defines folds from *indents devided by `shiftwidth`*.
Empty or white lines and lines starting with `foldignore` get the fold level of the line above or below it, instead of counting its indent.

**Expr** option enables `foldexpr` option. The `foldexpr` contains an expression that is evaluate on each line to get the fold level.
Examples:
    `:set foldexpr=getline(v:lnum)[0]==\"\\t\"` - all consecutive lines that start with a tab
    `:set foldexpr=MyFoldLevel(v:lnum)` - call custom function to compute the fold level
    `:set foldexpr=getline(v:lnum)=~'^\\s*$'&&getline(v:lnum+1)=~'\\S'?'<1':1`
        - make a fold out of paragraphs separated by blank lines
    `:set foldexpr=getline(v:lnum-1)=~'^\\s*$'&&getline(v:lnum)=~'\\S'?'>1':1`
        - ALSO make a fold out of paragraphs separated by blank lines
*Expression evaluation*
 - current buffer and window are set for the line
 - variable `v:lnum` is set to the line number
 - result specifies the fold level:
    - `0`                   = line is not in a fold
    - `1`, `2`, ..            = line is in a fold with this level
    - `-1`                  = fold level is undefined, use the fold level of line before or after
    - `"="`                 = use fold level from previous line
    - `"a1"`, `"a2"`, ..      = add one, two, .. to the fold level of the previous line
    - `"s1"`, `"s2"`, ..      = subtract one, two, .. from the fold level of the previous line
    - `"<1"`, `<2`, ..        = a fold with this level ends at this line
    - `">1"`, `">2"`, ..      = a fold with this level starts at this line

**Syntax** option means that folds are defined by syntax items that have the "fold" argument.

**Diff** option automatically defines folds for text that is not part of a change or close to a change.

**Marker**s in text tell where folds start and end. This allows you to precisely specify the folds withing the text itself. The `foldtext` option is normally set so that the text before the marker shows up in the folded line, which gives you the option go give a name to the fold.
`foldmarker` option specifies the marker string (default: "{{{,}}}").

Markers can have levels included (e.g. `{{{1`, `{{{2`, ...), or you can use matching pairs (e.g. `{{{` +`}}}`).

### Opening and Closing folds
- zo            = open one fold under cursor
- zO            = open all folds under cursor recursively
- zc            = close one fold under the cursor
- zC            = close all folds under the cursor recursively
- za            = toggle fold; close open fold, or open closed fold
- zA            = toggle all folds recursively
- zR            = open all folds

### Moving over folds
- [z            = move to the start of current fold, or start of parent fold
- ]z            = move to the end of the current open fold, or end of parent fold
- zj            = move to start of next fold
- zk            = move to end of previous fold

## Mode switching
- `<Esc>` = normal mode
- `i` = insert mode
- `v` = visual mode

## Operators
Operators are combined with any motion command, to perform the operation on a selected text
- `d` = delete
- `c` = change (delete + Insert mode)
- `y` = yank/copy

Special operator commands:
- `x` = delete character under cursor
- `X` = delete character left of cursor
- `dd` = delete whole line
- `D` = delete from here to end of line
- `cc` = change whole line
- `C` = change from here to end of line

## Cursor movement
**Character movement**
- `h`/`j`/`k`/`l` = left/down/up/right

**Word movement**
- `w`/`e` (`W`/`E`) = word begining forward/word end forward
- `b`/`ge` (`B`/`gE`) = word beginning backward/word end backward
 
**Line movement**
- `0`/`$` = beginning of line/end of line

**Move to specific line**
- `<line_num>G` = move to `<line_num>` line
- `gg` = move to first line
- `G` = move to last line
- `<perc>%` = move to `<perc>` percentage line
- `H`/`M`/`L` = move to first/middle/last line on the screen

**Single character search**
- `f<char>`/`F<char>` = next occurance of `<char>`/previous occurance of `<char>`
  - `<n>f<char>` = `n`-th occurance of `<char>`
- `t<char>` = position before the next occurance of `<char>`

**Matching parenthesis**
- `%` = go to matching parenthesis ()/[]/{}

## Window movement
- `CTRL-Y`/`CTRL-E` = move up/down single line
- `CTRL-U`/`CTRL-D` = move up/down half a screen
- `CTRL-B`/`CTRL-F` = move up/down full screen
- `zz`/`zt`/`zb` = center/top/bottom window at cursor position

## Jumping around
Each of the position you jump between is called a MARK.
- ```` = two backticks return you to a position before the last JUMP
  - JUMP is any movement further than within a line 
- `CTRL-O` = jump to previous position

**Named marks**
- `m<mark_letter>` = place a NAMED MARK at current place of cursor
  - mark letter can be anything `a-z` (26 marks)
- "\`<markletter>" = jump to mark `<mark_letter>`
- `'<mark_letter>` = jump to the begining of a line where `<mark_letter>` is

**Special marks**
- `'` = cursor position before doing a jump
- `"` = cursor position when last editing the file
- `[`/`]` = start/end of the last change

## Searching
All search operations support regular expressions for specifing search patterns!
- `/<string>` = search for specific `string`
  - `n`/`N` = next/previous match
  - `UP`/`DOWN` = move through search history
    - start typing then press `UP` to autocomplete the search term
- `?<string>` = sarch backward for specific string
- `*`/`#` = search the next/previous occurance of the word under currsor

Special search characters:
* `\<`/`\>` = match start/end of a word

There are several ways to **modify a search operation**:
- `ignorecase`/`noignorecase` = ignore case-specificity/enforce case-specificity
- `hlsearch`/`nohlsearch` = highlight all search matches
- `incsearch` = search for matches while you are still typing

### Custom config
- `n`=`nzzzv` | `N`=`Nzzzv`
- `,<Space>`=`:nohlsearch`

## Folding
Any text can be separated into sections, which can be collapesed into a single line.
Folding is useful because it allows you see a better overvie of the text.

Folded sections can be yanked, deleted, and pasted!

**Folding methods**:
There are several ways to define folds in a file
1. `manual` - using commands you define folds in a file
  - once you close a file, defined folds are lost
    - use `:mkview` and `:loadview` to save and load folds
2. `indent` - each lines fold is defined according to indent of each line (indent/`shiftwidth`)
3. `expr` - each lines fold is defined according to `foldexpr`
  - Examples:
    - `:set foldexpr=getline(v:lnum)[0]==\"\\t\"` = starts with tab
    - `:set foldexpr=MyFoldLevel(v:lnum)` = call custom function to computer fold level
    - `:set foldexpr=getline(v:lnum)=~'^\\s*$'&&getline(v:lnum+1)=~'\\S'?'<1':1` = folds separated by blank lines
    
4. `syntax`
5. `diff`
6. `marker`

**Folding commands**:
All folding commands start with `z`
- `zf` = create fold
- `zd`/`zD` = delete a fold/delete all folds
- `zi`/`zN`/`zn` = toggle/enable/disable all folds
- `zo`/`zO` = open fold/open all folds at cursor
- `zc`/`zC` = close fold/close all folds at cursor
- `zr` = reduce fold (open all folds of current level)
  - `zR` = reduce all nested folds
- `zm` = more fold (close all folds of current level)
  - `zM` = close all subfolds

There are several **methods to specify folding sections**.
Folding method is defined in `foldmethod` variable.
1. "manual"
2. "marker"
3. "expr"
4. "syntax"
5. "diff"

**Fold marks** in the gutter: `foldcolumn=4`.

### Workflow usage
1. Create folds to make the text more readable and navigatable. 
2. Switch between folded and unfolded view for moving around and editing.
 

## Tag jumping
To enable tag jumping inside your project you first need to generate a `tags` using the the command: `ctags -R .x or inside Vim `:command! MakeTags !ctags -R .

This `tags` file should be updated on any major source code change (whether manual or git pull). You can automate this update by configuring it inside the `.vimrc` file !!!

Once tags is created you can navigate using:
 - `:tag {name}` jump to specific tag
 - `^]`/`<C-LeftMouse>` go to definition 
 - `g^]` find usages
 - `^t` jump back up the tag stack


## Sessions
To save your project workbench (windows organizations and opened files) you use **Sessions**. Sessions contain all the information about what you are editing, file list, window layout, global variables, options and other information.
 - `sessionoptions` controls what exactly is remembered

**Creating a session** = `:mksession <session_filename>`
**Open a session** = `:source <session_filename>` | (from terminal) `vim -S <session_filename>`

Session information:
 - *blank* - keep empty windows
 - *buffers* - all buffers, not only ones in a window
 - *curdir* - current directory
 - *folds* - folds, also manually created ones
 - *help* - the help window
 - *options* - all options and mappings
 - *tabpages* - all tab pages
 - *winsize* - window sizes
 - *terminal* - include terminal windows

Sessions **do NOT store** position of *marks*, contents of *registers* and *command line history*. If you wish to save these things as well you need to use **viminfo** feature.

**Save viminfo** = `:wviminfo! <viminfo_filename>`
**Restore viminfo** = `:rviminfo! <viminfo_filename>`

### Usage
An obvious usage of sessions is when you are working on different projects. You can either setup a session at the start of the project and have a clean **project starting point session** for every time you come to the project, or you can have a **continuous session** that you update every time you leave a session so you can pick up where you left off.
**Load a project**
 1. `:source <session_filename>` | `vim -S <session_filename>`
**Leave a project**
 1. `:wall` - save all buffers
 2. `:mksession! <session_filename>` - save session (overrides previous session)
 
You can also create some specific session layouts that you use often. Like for example, you can create a session `mine.vim` that has a default layout you use for your daily notetaking, or a project template session from which you start to create new **project starting point session**. 


## Swap files
A `.swp` file is a file where Vim keep changes in while editing. In other words the `.swp` file is present only if a Vim instance is editing a file (or Vim crashed and didn't remove the `.swp` file).
If you open a file and it's `.swp` equivalent is present, Vim will keep changes. in `.swo` file.

# Plugins
**Installing plugin**
1. add a `Plug <plugin/url>` into your `.vimrc` file
2. save changes `:w`
3. `:source %`
4. `:PlugInstall`

**Removing plugin**
1. remove a `Plug <plugin/url>` line from your `.vimrc` file
2. restart Vim `:source %`
3. `:PlugClean`

## Goyo
Distraction free writing!

- `:Goyo` =  turn on fullscreen, uncluttered "distraction free" view
  - you can specify width and hight of "distraction free" view (e.g. `120` - width, `x30` - hight, `120x30` - hight)
- `:Goyo!` = turn of "distraction free" view

## ALE
Asynchronous lint engine!
Run linters on the content of text bufffer and provide warnings and errors as you type.

Linting options:
- `:help ale-options` 
- `:help-ale-integration-options`

Fixing 
