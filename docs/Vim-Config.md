# My Config
1. List of all config files can be listed with `:scriptnames` command

## UI
- line numbers: `:set number`/ `:set nonumber`
- display cursor postions in lower right: `:set ruler`

## Search config
If there are no capital letters in a search pattern = ignorecase and match a string with any capitalization that matches the pattern
If there are capital letters in a serch pattern = match only the exact match
- `:set ignorecase`/`:set noignorecase`

Incremental Search. Match as you type. Don't wait for <Enter> to execute the search.
- `:set incsearch`

Highlight all search matches.
- `:set hlsearch`/`:set nohlsearch`
- `:nohlsearch` = diable highlighting for current search

Wrap around the file when you walk through matches. A.k.a. `n` (next match) goes to the first match in file when you are one the last match.
- `:set wrapscan`/`:set nowrapscan`

## Plugins
### File Navigation
- NERDTree      ('scrooloose/nerdtree')

### UI
- Tagbar        ('majutsushi/tagbar')
- Vim-airline   ('vim-airline/vim-airline', 'vim-airline/vim-airline-themes')
- vim-gitgutter ('airblade/vim-gitgutter')
- indentline    ('yggdroot/indentline')

### Search
- grep          ('yegappan/grep')
- fzf           ('junegunn/fzf.vim')

### Notes
- Snippets      ('sirver/ultisnips', 'honza/vim-snippets')
- Notes         ('vimwiki/vimwiki', 'michal-h21/vim-zettel')

### Command Execution
- gmake         ('shougo/vimproc.vim', {'do': g:make})

### Editting
- delimitmate   ('raimondi/delimitmate')
- NerdCommenter ('scrooloose/nerdcommenter')
"- Git
"   - vim-fugitive ('tpope/vim-fugitive', 'tpope/vim-rhubarb')
"- Tmux
"   - vimux ('benmills/vimux')
"- Markdown
"
"- Python
"   - ale ('dense-analysis/ale')
"   - jedi-vim ('davidhalter/jedi-vim')
"   - requirements ('raimon49/requirements.txt.vim')
"- C
"- c.vim ('vim-scripts/c.vim', {'for': ['c', 'cpp']})
"- ('ludwig/split-manpage.vim')
"- ('fatih/vim-go', {'do': ':GoInstallBinaries'})
"- ('hail2u/vim-css3-syntax')
"- ('gko/vim-coloresque')
"- ('tpope/vim-haml')
"- ('mattn/emmet-vim')
"- ('jelera/vim-javascript-syntax')


## Views
While sessions store the look of the whole Vim, **Views** are used to store properties of one windows only. It stores one-off *options* you use, *fold* you defined.

### Usage
You can let vim keep track of View on per-file basis. By simply using `:mkview` on a file, and later `:loadview` on that same file, Vim will save and restore the a view for that file.
You can also have multiple views on per-file basis by using `mkview` and `loadview` and passing a number as an argument. Ex: `:mkview 1` and `:loadview 1` - stores and restore a new separate view from the one created by `:mkview`. These two view can have different options set, and different folds opened. *You can have up to 10 view per file (1-9) plus unnumbered one.*

If you want a View that isn't file specific you can store and restore a view using a specified name. Ex: `:mkview <view_filename>` and `:loadview <view_filename>.`

## Global searching
Used plugins:
 - `junegunn/fzf` / `junegunn/fzf.vim`
 - `yegappan/grep` = an interface towards any awailable *grep-like tool*
    - simply start with the coresponding command for a desired tool
    - `:Rg` = *Ripgrep* (https://github.com/BurntSushi/ripgrep)
    - `:Ag` = *SilverSearcher*
    - `:Gitgrep` = *Git grep*

### Usage
 1. To search for a file inside your project use `:Files` (fzf command)
    - interactive fuzzy search of filenames. Provides a better experience that Vim `:find` because it autocompletes as you write, and it fuzzy matches your query so you don't have to remember the filename exactly
    - it also provides a preview of files
    - <Ctrl-T> open file in new tab
    - <Ctrl-V> open in vertical split
 2. Use `:BLines`, instead of Vim `/`, to search through the current buffer text fuzzily.
 3. Use `:Lines` to search through all buffers text fuzzily.:

### Fzf
https://github.com/junegunn/fzf | https://github.com/junegunn/fzf.vim
A fuzzy finder that can take any input and allow you to search and select over that input.

**Searching**
 - Normally it is a fuzzy search so any entry that contains the given charactes anywhere is a match(but exact matches are at the top the list).
 - Prefix with ' to search only exact matches
 - Multiple search terms are delimited by <Space>
 - For matching at the end of a string use $ at end of your search term
 - negate search term with !
**Usage**
1. Pipe any command into fzf, filter input using interactive search, and select items
2. just `fzf` uses the FZF_DEFAULT_COMMAND which by default is `find`
3. select multiple items using <Tab>
4. Use `**` in filepath expressions to start fuzzy search on the filesystem
    - alternatively use <CTRL-T>
5. Use `**` with `ssh` to fuzzy search IP addresses (recently used and ssh.config)
6. Search command history

## Python
To make Vim a comprehensive tool for Python development there are several things we need:
 - syntax highlighting
 - *autocompletion*
    - <C-Space> = Completion
    - <leader>g = GoTo assignment
    - <leader>d = GoTo definition. Includes imports and statements
    - <leader>s = GoTo stub
    - `K` = show documentation
    - <leader>r = Rename
    - <leader>n = Find usages
    - `:PyImport <module>` = open module

To get all these features in Vim we rely on several plugins and Vim features we need to configure.
For autocompletion we rely on **jedi-vim** plugin.

