# Git
> Brach early, and branch often
Branches have almost no memory footprint, you should devide your work as much as possible. While still mainting some sort of organizational structure.
    - Experimental branches

## Basic usage

## Advanced usage

---
## Internals

### Git Objects

### Tree Objects

# vim-fugitive + vimdiff
Better use of Git through Vim.

## `:Git` or `:G`
Call any arbitrary Git command, like `:Git commit`, `:Git rebase -i`. It simply passes through the call to Git and echo its output.
- `:Git commit`, `:Git rebase -i`, ... invoce and editor in current Vim instance.
- `:Git diff`, `:Git log`, ... load buffer to a temporary buffer

### `:Git` no args
Bring up a summary akin to `git status` but INTERACTIVE.
- Use `-` key to stage and unstage individual files
- For staging all files at once use `:Git add .`
- `<Enter>` = open file for review
- `p` (`git add --patch`) - show file as chunks of changes

### `:Gitdiff`


### `:Git commit`


## `:GDelete` and `:GRemove`
Do `git rm` to stage a delete of a file from repository
& clear out the Vim buffer of that file.
`GDelete` deletes the buffer totaly, while `GRemove` keeps the empty buffer around.

## `:Gmove`
`git mv <old_filepath> <target_filepath>`, plus clearing of the old buffer and opening of the new buffer
- `:Git <target_path>` = path is relative to the old filepath
- `:Git /<target_path>` = root is the root of Git repository (not root of filesystem)



