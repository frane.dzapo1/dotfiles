# Personal Dotfiles

## Tasks
 - [ ] Zsh OS specific options section
 - [ ] Separate aliases from .zshrc into .aliases file
 - [ ] add IPython alias (at hand IPython instance)
     ```bash
     ipython --no-banner --TerminalInteractiveShell.editing_mode=vi --InteractiveShellApp.extensions="['autoreload']" --InteractiveShellApp.exec_lines="['%autoreload 2','import numpy as np','import matplotlib.pyplot as plt']"
     ```
 - [ ] add vim mode to Jupyter
 - 

## Description

### Path settings
`PATH` environment variable is used 

## Log

### ? Separating an entire .zshrc file into separate `*.zsh` files
It is possible to extract the entire content of a .zshrc file into seprate `*.zsh` files, and include the content of these files into the `.zshrc` by adding a `source ~/*/*.zsh` command. This way, all that is left in the `.zshrc` is a list of these `source` commands.
Alternatively one can place all into the `.zshrc` a comment seperate sections.

### Vim Navigation
- In file navigation
  - String search (`/`, fzf)
- In project navigation
  - Tag jumping
  - File jumping
  - Project-level string search (grep, ...)

