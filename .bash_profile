# Ensure ~/.bash/env gets run first
. ~/.bash/env

# Run ~/.bash/login
. ~/.bash/login

# Run ~/.bash/interactive if this is an interactive shell
if [ "$PS1" ]; then
    . ~/.bash/interactive
fi
